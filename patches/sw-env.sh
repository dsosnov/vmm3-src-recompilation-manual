SWPATH="`realpath "#SWPATH"`"

. "$SWPATH"/root/bin/thisroot.sh

LD_LIBRARY_PATH="$SWPATH"/fmt/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/graylog-logger.sh/lib:"${LD_LIBRARY_PATH}"

LD_LIBRARY_PATH="$SWPATH"/h5cpp/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/date/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/qplot/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/logical-geometry/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/flatbuffers/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/qt-color-widgets/lib:"${LD_LIBRARY_PATH}"

LD_LIBRARY_PATH="$SWPATH"/vmm-sdat/lib:"${LD_LIBRARY_PATH}"
LD_LIBRARY_PATH="$SWPATH"/essdaq/daquiri/daquiri/lib:"${LD_LIBRARY_PATH}"

PATH="$SWPATH"/flatbuffers/bin:"${PATH}"

PATH="$SWPATH"/vmmsc/build:"${PATH}"
PATH="$SWPATH"/vmm-sdat/bin:"${PATH}"

PATH="$SWPATH"/essdaq/daquiri/daquiri/bin:"${PATH}"
# PATH="$SWPATH"/essdaq/kafka:"${PATH}"
PATH="$SWPATH"/essdaq/kafka/kafka/bin:"${PATH}"
# PATH="$SWPATH"/essdaq/efu:"${PATH}"
PATH="$SWPATH"/essdaq/efu/event-formation-unit/bin:"${PATH}"
PATH="$SWPATH"/essdaq/grafana/grafana/bin:"${PATH}"

LD_LIBRARY_PATH="$SWPATH"/vmm_readout_software/build:"${LD_LIBRARY_PATH}"
PATH="$SWPATH"/vmm_readout_software/build:"${PATH}"

export PATH
export LD_LIBRARY_PATH

export ESSDAQROOT="$SWPATH"

# Disabling leak sanitizer to prevent massive output from vmmsc
export ASAN_OPTIONS=detect_leaks=0

# Aliases
alias essws='wireshark -x lua_script:"$SWPATH"/essdaq/wireshark/lua/vmm3a.lua'
alias kafka_start="${SWPATH}/essdaq/kafka/start_kafka.sh"
alias kafka_stop="${SWPATH}/essdaq/kafka/stop_kafka.sh"
alias kafka_check="${SWPATH}/essdaq/kafka/check_kafka.sh"
for i in `ls -1 "$SWPATH"/essdaq/detectors/`; do
  alias essdaq_${i}_start="${SWPATH}/essdaq/detectors/${i}/start.sh"
  alias essdaq_${i}_stop="${SWPATH}/essdaq/detectors/${i}/stop.sh"
  alias essdaq_${i}_dump="${SWPATH}/essdaq/detectors/${i}/dump_start.sh"
  alias daquiri_${i}_start="${SWPATH}/essdaq/detectors/${i}/daquiri.sh"
done
