find_path(readerwriterqueue_INCLUDE_DIR
  NAMES readerwriterqueue/readerwriterqueue.h
  PATHS /usr/local/include/ /opt/local/include/
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(readerwriterqueue FOUND_VAR readerwriterqueue_FOUND REQUIRED_VARS
  readerwriterqueue_INCLUDE_DIR
)

mark_as_advanced(
  readerwriterqueue_INCLUDE_DIR
)

if(readerwriterqueue_FOUND)
    set(readerwriterqueue_INCLUDE_DIRS ${readerwriterqueue_INCLUDE_DIR})
endif()

if(readerwriterqueue_FOUND AND NOT TARGET readerwriterqueue::readerwriterqueue)
    add_library(readerwriterqueue::readerwriterqueue INTERFACE IMPORTED)
    set_target_properties(readerwriterqueue::readerwriterqueue PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${readerwriterqueue_INCLUDE_DIR}"
            )
endif()
