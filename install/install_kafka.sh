#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

if [ ! -d "essdaq" ]; then
    . "${SHPATH}/install_essdaq.sh"
    if [ ! -d "essdaq" ]; then
        echo "No ESSDAQ installed! Exiting..."
        return
    fi
fi

cd essdaq/kafka
kafka=kafka_2.13-2.5.0
kafkafile=$kafka.tgz
kafkaurl=https://archive.apache.org/dist/kafka/2.5.0
wget "${kafkaurl}/${kafkafile}"
if [ ! -f $kafkafile ]; then
    cp "${SHPATH}/../archives/${kafkafile}" ./
fi
tar xf $kafkafile
rm -f  $kafkafile

ln -s $kafka kafka

# Next lines from install.sh file
# Create and set zookeeper data directory in configuration file
mkdir -p "${kafka}/zookeeper_data"
sed -e 's/dataDir=.*/dataDir=zookeeper_data/' -i $kafka/config/zookeeper.properties
# Try to  add Kafka IP address to config file
# Fist get it via the user configuration - if it exist
if [ -f "../config/system.sh" ]; then # if test -f "../config/system.sh"; then
    # get config variables
    . ../config/system.sh
    IP=$KAFKA_IP
fi
# If it doesn't then use localhost
if [[ $IP == "" ]]; then
    echo "No KAFKA_IP, setting IP to 127.0.0.1 (change with setlistener.sh)" | tee -a $LOGFILE
    IP="127.0.0.1"
fi
echo "Kafka IP address: "$IP | tee -a $LOGFILE
sed -e "s/^advertised.listeners=.*/advertised.listeners=PLAINTEXT:\/\/$IP:9092/g" -i".bak" server.config
# end lines from install.sh
cd ../..

cd "$dir0"
