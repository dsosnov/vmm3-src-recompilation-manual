#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules -b v1.12.0 https://github.com/google/flatbuffers.git flatbuffers_src --depth=1
mkdir flatbuffers_build; cd flatbuffers_build
cmake ../flatbuffers_src/ -DCMAKE_INSTALL_PREFIX=../flatbuffers
cmake --build . -- -j4
cmake --build . --target install
cd ..
export FLATBUFFERSDIR="`realpath .`/flatbuffers"

cd "$dir0"
