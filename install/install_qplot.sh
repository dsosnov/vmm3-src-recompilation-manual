#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules https://github.com/ess-dmsc/qplot qplot_src
cd qplot_src
git checkout 2ffc74f
cd ..

mkdir qplot_build; cd qplot_build
cmake ../qplot_src/ -DCMAKE_INSTALL_PREFIX=../qplot
cmake --build . -- -j4
cmake --build . --target install
cd ..
export QPLOTDIR="`realpath .`/qplot"

cd "$dir0"
