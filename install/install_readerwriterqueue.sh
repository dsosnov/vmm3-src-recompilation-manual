#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules -b v1.0.4 https://github.com/cameron314/readerwriterqueue readerwriterqueue_src
# cd readerwriterqueue_src
# git checkout 07e22ec # commit from Commits on Feb 20, 2018
# # may be it will be better to use Commits on Oct 1, 2020 - 02ec690
# # Or version v1.0.4 (from Feb 24, 2021)
# cd ..

mkdir readerwriterqueue_build; cd readerwriterqueue_build
cmake ../readerwriterqueue_src/ -DCMAKE_INSTALL_PREFIX=../readerwriterqueue
cmake --build . -- -j4
cmake --build . --target install
cd ..
export READERWRITERQUEUEDIR="`realpath .`/readerwriterqueue"

cd "$dir0"
