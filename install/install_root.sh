#!/usr/bin/env bash

dir0="`pwd`"

pathToRoot=${1:-"/opt"}
currentRootVersion="v"${2:-"6-24-00"} # 6-22-06

realPathToRoot=`realpath "${pathToRoot}"`
realPathToRoot_Install="${realPathToRoot}/root"

realPathToRoot_Src="${realPathToRoot}/root_src_${currentRootVersion}"
realPathToRoot_Build="${realPathToRoot}/root_build"

mkdir -p "$realPathToRoot_Install" "$realPathToRoot_Src" "$realPathToRoot_Build"
[ "$?" != "0" ] && exit 1

git clone -b "${currentRootVersion}" --single-branch http://root.cern.ch/git/root.git "${realPathToRoot_Src}" --depth=1
[ "$?" != "0" ] && exit 1

cd "$realPathToRoot_Build"

defaultRootOptions="-DCMAKE_CXX_STANDARD=17 -Dr=ON -Dtbb=ON -DPYTHON3_EXECUTABLE=/bin/python3 -DPYTHON2_EXECUTABLE=/bin/python2" # -Dxrootd=OFF -Dbuiltin_xrootd=OFF
cmake $defaultRootOptions -DCMAKE_INSTALL_PREFIX="$realPathToRoot_Install" "$realPathToRoot_Src"
[ "$?" != "0" ] && exit 1
cmake --build . -- -j2
[ "$?" != "0" ] && exit 1
cmake --build . --target install
[ "$?" != "0" ] && exit 1

cd "$dir0"
