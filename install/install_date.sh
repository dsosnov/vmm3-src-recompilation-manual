#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules https://github.com/HowardHinnant/date.git date_src
cd date_src
git checkout 27d1e1e54e4d120f421710df2040f54d51527c21
cd ..

mkdir date_build; cd date_build
cmake ../date_src/ -DCMAKE_INSTALL_PREFIX=../date -DUSE_SYSTEM_TZ_DB=ON
cmake --build . -- -j4
cmake --build . --target install
cd ..
export DATEDIR="`realpath .`/date"

cd "$dir0"
