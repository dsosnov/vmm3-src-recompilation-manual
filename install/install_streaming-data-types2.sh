#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

git clone --recurse-submodules https://github.com/ess-dmsc/streaming-data-types.git streaming-data-types_416487f_src
cd streaming-data-types_416487f_src
git checkout 416487f # for efu
git apply "$SHPATH"/../patches/streaming-data-types_CmakeLists.patch
cp "$SHPATH"/../patches/streaming-data-typesConfig.cmake.in streaming-data-typesConfig.cmake.in
cd ..
mkdir streaming-data-types_416487f_build; cd streaming-data-types_416487f_build
cmake ../streaming-data-types_416487f_src/ -DCMAKE_INSTALL_PREFIX=../streaming-data-types_416487f -DFlatbuffers_DIR="${FLATBUFFERSDIR}"/lib/cmake/flatbuffers
cmake --build . -- -j4
cmake --build . --target install
cd ..
export STREAMINGDATATYPES2DIR="`realpath .`/streaming-data-types_416487f"

cd "$dir0"
