#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules https://github.com/cameron314/concurrentqueue.git concurrentqueue_src
cd concurrentqueue_src
git checkout 8f7e861
cd ..
mkdir -p concurrentqueue/include/concurrentqueue
cp concurrentqueue_src/*.h concurrentqueue/include/concurrentqueue
export CONCURRENTQUEUEDIR="`realpath .`/concurrentqueue"

cd "$dir0"
