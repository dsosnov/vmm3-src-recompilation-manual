#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

git clone --recurse-submodules https://github.com/cameron314/concurrentqueue.git concurrentqueue_src
cd concurrentqueue_src
git checkout 8f7e861

cp "$SHPATH"/../patches/concurrentqueue_CMakeLists.txt CMakeLists.txt
cp "$SHPATH"/../patches/concurrentqueueConfig.cmake.in concurrentqueueConfig.cmake.in

cd ..

mkdir concurrentqueue_build; cd concurrentqueue_build
cmake ../concurrentqueue_src/ -DCMAKE_INSTALL_PREFIX=../concurrentqueue
cmake --build . --target install -- -j4
cd ..
export CONCURRENTQUEUEDIR="`realpath .`/concurrentqueue"

cd "$dir0"
