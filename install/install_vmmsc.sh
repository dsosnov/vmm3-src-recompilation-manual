#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules https://gitlab.cern.ch/rd51-slow-control/vmmsc.git vmmsc
cd vmmsc
git checkout e72fdf032bc206bc0d0f93d662c2a7a25ffc657a
git submodule update --recursive

cd build
# qmake -set prefix ../../vmmsc
rm -f .qmake.stash
qmake vmmdcs.pro # PREFIX=../vmmsc
make -j4
cd ../../

cd "$dir0"
