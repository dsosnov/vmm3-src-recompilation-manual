#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

git clone --recurse-submodules  -b v1.3.1 https://github.com/gabime/spdlog.git spdlog_src --depth=1

cd spdlog_src
git apply "$SHPATH"/../patches/spdlog_pattern_formatter.patch
cd ..

mkdir spdlog_build; cd spdlog_build
cmake ../spdlog_src/ -DCMAKE_INSTALL_PREFIX=../spdlog -DSPDLOG_BUILD_BENCH=OFF
cmake --build . -- -j4
cmake --build . --target install
cd ..
export SPDLOGDIR="`realpath .`/spdlog"

cd "$dir0"
