#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules https://gitlab.com/mattbas/Qt-Color-Widgets.git qt-color-widgets_src
cd qt-color-widgets_src
git checkout a95f72e --recurse-submodules
cd ..

mkdir qt-color-widgets_build; cd qt-color-widgets_build
cmake ../qt-color-widgets_src/ -DCMAKE_INSTALL_PREFIX=../qt-color-widgets
cmake --build . -- -j4
cmake --build . --target install
cd ..
export QTCOLORWIDGETSDIR="`realpath .`/qt-color-widgets"

cd "$dir0"
