#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

# sudo apt-get install libpcap0.8-dev libpcap0.8-dbg libpcap0.8 libpcap-dev

git clone --recurse-submodules https://github.com/ess-dmsc/vmm-sdat.git vmm-sdat_src
cd vmm-sdat_src
git checkout b74a8154c69b5d6ebbf39c37b5bbda5b3f2ae040
git submodule update --recursive # doesnt need since it hasnt submodules

git apply "$SHPATH"/../patches/vmm-sdat-noconan.patch
git apply "$SHPATH"/../patches/vmm-sdat_FindGTestFix.patch
git apply "$SHPATH"/../patches/vmm-sdat-cxx_standard.patch
cp "$SHPATH"/../patches/Findasio.cmake cmake/modules # cp ../graylog-logger_src/cmake/Findasio.cmake cmake/modules # For asio searching

cd ..

mkdir vmm-sdat_build; cd vmm-sdat_build

# echo "cmake ../vmm-sdat_src/ -DCMAKE_INSTALL_PREFIX=../vmm-sdat -DCONAN=DISABLE -DCONAN=DISABLE -Dnlohmann_json_DIR=${NLOHMANNJSONDIR}/lib/cmake/nlohmann_json -DASIO_INCLUDE_DIR=${AISODIR}/include -DASIO_ROOT_DIR=${AISODIR} -Dconcurrentqueue_INCLUDE_DIR=${CONCURRENTQUEUEDIR}/include -Dfmt_DIR=${FMTDIR}/lib/cmake/fmt/ -DGraylogLogger_DIR=${GRAYLOGLOGGERDIR}/lib/cmake/GraylogLogger"
cmake ../vmm-sdat_src/ -DCMAKE_INSTALL_PREFIX=../vmm-sdat -DCONAN=DISABLE -DCONAN=DISABLE -Dnlohmann_json_DIR=${NLOHMANNJSONDIR}/lib/cmake/nlohmann_json -DASIO_INCLUDE_DIR=${AISODIR}/include -DASIO_ROOT_DIR=${AISODIR} -Dconcurrentqueue_INCLUDE_DIR=${CONCURRENTQUEUEDIR}/include -Dfmt_DIR=${FMTDIR}/lib/cmake/fmt/ -DGraylogLogger_DIR=${GRAYLOGLOGGERDIR}/lib/cmake/GraylogLogger # -Dconcurrentqueue_DIR=${CONCURRENTQUEUEDIR}/lib/cmake/concurrentqueue
cmake --build . # -- -j4
cmake --build . --target install
cd ..

cd "$dir0"
