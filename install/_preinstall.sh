#!/usr/bin/env bash
packages=( dpkg-dev build-essential
           git
           binutils gcc g++
           make cmake
           python3
           libx11-dev libxpm-dev libxft-dev libxext-dev
           libpng-dev libpng++-dev libjpeg-dev libssl-dev
           python python3
           uuid-dev
           clang
           googletest
           autoconf
           libhdf5-dev
           libbenchmark-dev
         )
packages_boost=(libboost-dev
                libboost-filesystem-dev libboost-system-dev
                libboost-atomic-dev libboost-chrono-dev
                libboost-thread-dev
                # libboost-all-dev
               )
packages_vmmssc=( qt5-default qconf qt5-qmake libqt5serialport5 libqt5serialport5-dev qtserialport5-doc qtserialport5-doc-html ) # For vmmsc
packages_vmmsdat=( libpcap0.8-dev libpcap0.8-dbg libpcap0.8 libpcap-dev ) # For vmm-sdat
packages_essdaq=( libeigen3-dev libeigen3-doc ) # For essdaq
packages_kafka=( curl default-jre python3-pip ) # For Kafka
packages_daquiri=( qt5-default xutils-dev libtool librdkafka++1 librdkafka1 librdkafka-dev libspdlog-dev )
packages_wireshark=( wireshark-qt wireshark-gtk )

sudo apt install -y ${packages[@]} ${packages_boost[@]} \
     ${packages_vmmssc[@]} ${packages_vmmsdat[@]} \
     ${packages_essdaq[@]} ${packages_kafka[@]} ${packages_daquiri[@]} \
     ${packages_wireshark[@]}

sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 10
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 20

# Installing packages for Kafka (from install.sh)
sudo pip3 install -qq kafka-python argparse

# Set system kernel option for HW Daquiri check
echo '
# Options for Daquiri
net.core.rmem_max = 12582912
net.core.wmem_max = 12582912
net.core.netdev_max_backlog = 5000' | sudo tee -a /etc/sysctl.conf
# sysctl.conf options will work after reboot.
# To set the same for current computer boot:
sudo sysctl -w net.core.rmem_max=12582912
sudo sysctl -w net.core.wmem_max=12582912
sudo sysctl -w net.core.netdev_max_backlog=5000
