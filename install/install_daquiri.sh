#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

if [ ! -d "essdaq" ]; then
    . "${SHPATH}/install_essdaq.sh"
    if [ ! -d "essdaq" ]; then
        echo "No ESSDAQ installed! Exiting..."
        return
    fi
fi

cd essdaq/daquiri
git clone https://github.com/ess-dmsc/daquiri.git --recurse-submodules daquiri_src
cd daquiri_src
git checkout 7a6b68e399344a331f18160bfe5bada8154af635
git apply "$SHPATH"/../patches/daquiri_FindGTestFix.patch
git apply "$SHPATH"/../patches/daquiri_CMakeLists.patch

mv data/profiles/nmx/default_consumers.daq data/profiles/nmx/default_consumers.daq.bak
mv data/profiles/nmx/profile.set data/profiles/nmx/profile.set.bak
cp -f ../profiles/nmx/* data/profiles/nmx/
cd ..

mkdir daquiri_build; cd daquiri_build
cmake ../daquiri_src/ -DCMAKE_INSTALL_PREFIX=../daquiri -DCONAN=DISABLE \
      -Dfmt_DIR=${FMTDIR}/lib/cmake/fmt/ -Ddate_DIR=${DATEDIR}/lib/cmake/date \
      -Dh5cpp_DIR=${H5CPPDIR}/lib/cmake/h5cpp-0.4.0 -Dqplot_DIR=${QPLOTDIR}/lib/cmake/qplot-0.0.1 \
      -Dnlohmann_json_DIR=${NLOHMANNJSONDIR}/lib/cmake/nlohmann_json \
      -Dspdlog_DIR=${SPDLOGDIR}/lib/cmake/spdlog \
      -Dlogical_geometry_DIR=${LOGICALGEOMETRYDIR}/lib/cmake/logical_geometry \
      -DFlatbuffers_DIR=${FLATBUFFERSDIR}/lib/cmake/flatbuffers -Dstreaming-data-types_DIR=${STREAMINGDATATYPES1DIR}/lib/cmake/streaming-data-types \
      -DQtColorWidgets-Qt52_DIR=${QTCOLORWIDGETSDIR}/lib/cmake/QtColorWidgets-Qt52 \
      -DCLI11_DIR=${CLI11DIR}/lib/cmake/CLI11 # -DCMAKE_CXX_STANDARD=17
    
cmake --build . --target acquire -- -j2
cmake --build . --target daquiri -- -j2
# cmake --build . --target everything # -- -j2
cmake --build . --target install

cd ..

mkdir -p daquiri/build
ln -s ../bin daquiri/build/bin
ln -s ../lib daquiri/build/lib
ln -s "$SHPATH"/sw-env.sh daquiri/build/activate_run.sh

cd ..

cd "$dir0"
