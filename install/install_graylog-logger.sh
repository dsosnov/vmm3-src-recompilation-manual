#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules -b v2.0.3 https://github.com/ess-dmsc/graylog-logger.git graylog-logger_src --depth=1
mkdir graylog-logger_build; cd graylog-logger_build
cmake ../graylog-logger_src/ -DCMAKE_INSTALL_PREFIX=../graylog-logger -DCONAN=DISABLE -Dnlohmann_json_DIR="$NLOHMANNJSONDIR"/lib/cmake/nlohmann_json -DASIO_INCLUDE_DIR="$AISODIR"/include -DASIO_ROOT_DIR="$AISODIR" -Dconcurrentqueue_INCLUDE_DIR="$CONCURRENTQUEUEDIR"/include -Dfmt_DIR="$FMTDIR"/lib/cmake/fmt/ # -Dconcurrentqueue_DIR="$CONCURRENTQUEUEDIR"/lib/cmake/concurrentqueue
cmake --build . -- -j4
cmake --build . --target install
cd ..
export GRAYLOGLOGGERDIR="`realpath .`/graylog-logger"

cd "$dir0"
