#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

# sudo apt install libeigen3-dev libeigen3-doc

git clone https://github.com/ess-dmsc/essdaq.git --recurse-submodules essdaq
cd essdaq
git checkout a5cfbfe5823e74eda9bf5ed82c6e113b25537763
git apply "$SHPATH"/../patches/essdaq_daquiri_sh.patch
 
NETNAME=`lshw -class network | grep "logical name" | head -n 1 | awk '{print $3}'`
sed 's|#ETH0|'"$NETNAME"'|; s|#HOME|'"$HOME"'|' "$SHPATH"/../patches/essdaq_system.sh > config/system.sh

cd ..

cd "$dir0"
