#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

git clone --recurse-submodules https://github.com/ess-dmsc/logical-geometry.git logical-geometry_src
cd logical-geometry_src
git checkout 705ea61
git apply "$SHPATH"/../patches/logical-geometry_FindGTestFix.patch
git apply "$SHPATH"/../patches/logical-geometry_CMakeLists.patch
cp "$SHPATH"/../patches/logical_geometryConfig.cmake.in logical_geometryConfig.cmake.in

# There is a definition of testing::internal::GTestColor in googletest in Ubuntu 20.04
[[ "`lsb_release -c -s`" == "focal" ]] && git apply "$SHPATH"/../patches/logical-geometry_TestBase.patch
cd ..

mkdir logical-geometry_build; cd logical-geometry_build
cmake ../logical-geometry_src/ -DCMAKE_INSTALL_PREFIX=../logical-geometry -DCONAN=DISABLE
cmake --build . -- -j4
cmake --build . --target install
cd ..
export LOGICALGEOMETRYDIR="`realpath .`/logical-geometry"

cd "$dir0"
