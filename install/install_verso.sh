#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

########################################################################################################################
# vmm_readout_software is "internal" - The project can be accessed by any logged in user except external users.
# Use cloning in case you can access it
########################################################################################################################
# git clone --recurse-submodules https://gitlab.cern.ch/NSWelectronics/vmm_readout_software.git vmm_readout_software
# cd vmm_readout_software
# git checkout 592db48295ed22f4ef633f0127e3fc795be0cd15
# git apply "$SHPATH"/../patches/verso_pro.patch
# cd ..
########################################################################################################################

########################################################################################################################
# Or use tar archives...
########################################################################################################################
# Archive with git history
tar xf "$SHPATH"/../archives/vmm_readout_software.tar.gz
########################################################################################################################
# # Updated archive without git history
# tar xf "$SHPATH"/../archives/vmm_readout_software-master-upd.tar
# mv vmm_readout_software-master-upd vmm_readout_software
########################################################################################################################
# # Updated archive without git history
# tar xf "$SHPATH"/../archives/vmm_readout_software-master.tar
# mv vmm_readout_software-master vmm_readout_software
# cd vmm_readout_software
# patch "$SHPATH"/../patches/verso_pro.patch
# cd ..
########################################################################################################################

cd vmm_readout_software
bash install.sh

cd build

make -j4

cd ../../

cd "$dir0"
