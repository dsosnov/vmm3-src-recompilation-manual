#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules -b asio-1-16-1 https://github.com/chriskohlhoff/asio.git asio_src --depth=1
mkdir asio
cd asio_src/asio
./autogen.sh
./configure --prefix="`realpath ../../asio`"
make -j4
make install
cd ../..
export AISODIR="`realpath .`/asio"

cd "$dir0"
