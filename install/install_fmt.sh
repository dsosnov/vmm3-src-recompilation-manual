#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules -b 6.2.0 https://extgit.iaik.tugraz.at/conan/fmt.git fmt_src --depth=1
mkdir fmt_build; cd fmt_build
cmake ../fmt_src/ -DCMAKE_INSTALL_PREFIX=../fmt -DCMAKE_CXX_FLAGS="-fPIC"
cmake --build . -- -j4
cmake --build . --target install
cd ..
export FMTDIR="`realpath ./fmt`"

cd "$dir0"
