#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules  -b v40 https://github.com/rollbear/trompeloeil trompeloeil_src --depth=1

mkdir trompeloeil_build; cd trompeloeil_build
cmake ../trompeloeil_src/ -DCMAKE_INSTALL_PREFIX=../trompeloeil
cmake --build . -- -j4
cmake --build . --target install
cd ..
export TROMPELOEILDIR="`realpath .`/trompeloeil"

cd "$dir0"
