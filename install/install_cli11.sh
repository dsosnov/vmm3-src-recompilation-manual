#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules  -b v1.9.1 https://github.com/CLIUtils/CLI11 cli11_src --depth=1

mkdir cli11_build; cd cli11_build
cmake ../cli11_src/ -DCMAKE_INSTALL_PREFIX=../cli11
cmake --build . -- -j4
cmake --build . --target install
cd ..
export CLI11DIR="`realpath .`/cli11"

cd "$dir0"
