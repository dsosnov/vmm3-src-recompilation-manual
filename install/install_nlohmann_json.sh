#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

git clone --recurse-submodules -b v3.7.0 https://github.com/nlohmann/json.git nlohmann_json_src --depth=1
mkdir nlohmann_json_build; cd nlohmann_json_build
cmake ../nlohmann_json_src/ -DCMAKE_INSTALL_PREFIX=../nlohmann_json
cmake --build . -- -j4
cmake --build . --target install
cd ..
export NLOHMANNJSONDIR="`realpath .`/nlohmann_json"

cd "$dir0"
