#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

git clone --recurse-submodules https://github.com/ess-dmsc/h5cpp.git h5cpp_src # 
cd h5cpp_src
git checkout 6e47d9629f7b316e48eba601c54b96fc4880e4bd
git apply "$SHPATH"/../patches/h5cpp_FindGTestFix.patch
cd ..
mkdir h5cpp_build; cd h5cpp_build
cmake ../h5cpp_src/ -DCMAKE_INSTALL_PREFIX=../h5cpp -DCONAN=DISABLE
cmake --build . -- -j4
cmake --build . --target install
cd ..
export H5CPPDIR="`realpath .`/h5cpp"

cd "$dir0"
