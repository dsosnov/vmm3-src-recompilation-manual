#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

if [ ! -d "essdaq" ]; then
    . "${SHPATH}/install_essdaq.sh"
    if [ ! -d "essdaq" ]; then
        echo "No ESSDAQ installed! Exiting..."
        return
    fi
fi

cd essdaq/efu
git clone https://github.com/ess-dmsc/event-formation-unit.git event-formation-unit_src
cd event-formation-unit_src
git checkout eaf77593144b2a982b6d5117c49d2ce3605c06b1
cp "$SHPATH"/../patches/Findasio.cmake cmake/modules # For asio searching
cp "$SHPATH"/../patches/Findreaderwriterqueue.cmake cmake/modules
git apply "$SHPATH"/../patches/efu_CMakeLists.patch

# There is a definition of testing::internal::GTestColor in googletest in Ubuntu 20.04
[[ "`lsb_release -c -s`" == "focal" ]] && git apply "$SHPATH"/../patches/efu_TestBase.patch
cd ..

mkdir event-formation-unit_build; cd event-formation-unit_build
cmake ../event-formation-unit_src/ -DCMAKE_INSTALL_PREFIX=../event-formation-unit -DCONAN=DISABLE \
      -Dfmt_DIR=${FMTDIR}/lib/cmake/fmt/ -Dconcurrentqueue_INCLUDE_DIR=${CONCURRENTQUEUEDIR}/include \
      -Dh5cpp_DIR=${H5CPPDIR}/lib/cmake/h5cpp-0.4.0 \
      -DASIO_INCLUDE_DIR=${AISODIR}/include -DASIO_ROOT_DIR=${AISODIR} \
      -DCLI11_ROOT_DIR=${CLI11DIR} -DCLI11_INCLUDE_DIR=${CLI11DIR}/include \
      -DTrompeloeil_ROOT_DIR=${TROMPELOEILDIR} -DTrompeloeil_INCLUDE_DIR=${TROMPELOEILDIR}/include \
      -Dfmt_DIR=${FMTDIR}/lib/cmake/fmt/ -Ddate_DIR=${DATEDIR}/lib/cmake/date \
      -Dnlohmann_json_DIR=${NLOHMANNJSONDIR}/lib/cmake/nlohmann_json \
      -DFlatbuffers_DIR=${FLATBUFFERSDIR}/lib/cmake/flatbuffers -Dstreaming-data-types_DIR=${STREAMINGDATATYPES2DIR}/lib/cmake/streaming-data-types \
      -DASIO_INCLUDE_DIR=${AISODIR}/include -DASIO_ROOT_DIR=${AISODIR} \
      -DGraylogLogger_DIR=${GRAYLOGLOGGERDIR}/lib/cmake/GraylogLogger -Dconcurrentqueue_INCLUDE_DIR=${CONCURRENTQUEUEDIR}/include \
      -Dreaderwriterqueue_INCLUDE_DIR=${READERWRITERQUEUEDIR}/include \
      -Dlogical_geometry_DIR=${LOGICALGEOMETRYDIR}/lib/cmake/logical_geometry
      
cmake --build . -- -j2
cmake --build . --target install

cd ..

mkdir -p event-formation-unit/build
ln -s ../bin event-formation-unit/build/bin
ln -s "$SHPATH"/sw-env.sh event-formation-unit/build/activate_run.sh

cd ..

cd "$dir0"
