#!/usr/bin/env bash

# # from https://grafana.com/docs/grafana/latest/installation/debian/
# sudo ( \
#        apt-get install -y apt-transport-https software-properties-common wget
#        apt-key add "$SHPATH"/../patches/grafana-gpg.key
#        echo "deb https://packages.grafana.com/oss/deb stable main" >> /etc/apt/sources.list.d/grafana.list
#        apt-get update
#        apt-get install -y grafana
#     )

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

if [ ! -d "essdaq" ]; then
    . "${SHPATH}/install_essdaq.sh"
    if [ ! -d "essdaq" ]; then
        echo "No ESSDAQ installed! Exiting..."
        return
    fi
fi

cd essdaq/grafana

GRAFANA_VERSION="8.3.3";
GRAFANA_FOLDER="grafana-${GRAFANA_VERSION}"
GRAFANA_FILE="${GRAFANA_FOLDER}.linux-amd64.tar.gz"

wget "https://dl.grafana.com/oss/release/${GRAFANA_FILE}"
tar -xf "${GRAFANA_FILE}"

rm -rf "${GRAFANA_FILE}"

ln -s "${GRAFANA_FOLDER}" grafana

cd ../..

cd "$dir0"
