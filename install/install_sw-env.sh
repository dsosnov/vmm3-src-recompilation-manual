#!/usr/bin/env bash

dir0="`pwd`"
SWPATH=${SWPATH:-"`realpath ~/`"}
cd "$SWPATH"

SHPATH=${SHPATH:-"`realpath ~/`"}

sed 's|#SWPATH|'"$SWPATH"'|' "$SHPATH"/../patches/sw-env.sh > "$SWPATH"/sw-env.sh

cd "$dir0"
