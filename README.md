1. **Manual**

    The link to the gitlab repository with manual: <https://gitlab.cern.ch/dsosnov/vmm3-src-recompilation-manual>

    It was checked on virtual machines with Ubuntu LTS 18.04 and 20.04 (minimal itstallation).<br>
    Minimal installation — is installation option commented as '_web browser and basic utilites_'. (see screenshots in attachment)<br>
    No updates was specifically installed during or after installation.<br>
    The archives with virtual machines can be found by the link: [_CERNBox links provided by mail_; username and password: `vmm3a`] (17 and 19 GB, compressed to 4.8 and 5.6 GB 7z-archives)

2. **Conan package manager**

    It is important to say, that the manual was created to work without conan package manager since it is thing-in-itself and there is no way to manage problems during compilation procedure.

3. **System requirements**

    The ROOT and event-formation-unit installation takes over 2.5 GB of memory, so for the installation procedure Ubuntu minimal requirement may be used: non the less of 4GB of sum of memory and swap space.<br>
    (Ubuntu system requirements can be found by the link: https://help.ubuntu.com/community/Installation/SystemRequirements)

4. **Installation**

    The main installation script "**_install_all.sh**" installs programs for VMM3a: ROOT, vmmsc, vmm-sdat and essdaq with all dependences (from repository also).<br>
    The installation of updated software for VMM3, _verso_, can be done with "**install/install_verso.sh**" script.<br>
    It requires ROOT and repository dependences installed ("**_preinstall.sh**" script) also with SWPATH and SHPATH set variables.<br>

    It is necessary to check after Daquiri installation that "**essdaq/config/system.sh**" file contains correct network device name (variable **UDP_ETH**) and path for dumps (variable **DUMP_PATH**)<br>

5. **Program versions (commits)**

    The versions of essdaq, efu and daquiri was selected accordingly with Lucian's mail from 07.09.2021:
    - essdaq  : a5cfbfe5823e74eda9bf5ed82c6e113b25537763
    - efu     : eaf77593144b2a982b6d5117c49d2ce3605c06b1
    - daquiri : 7a6b68e399344a331f18160bfe5bada8154af635

    For the vmmsc and vmm-sdat the version from Tassos pc was used (as I remember):
    - vmmsc   : e72fdf032bc206bc0d0f93d662c2a7a25ffc657a
    - vmm-sdat: b74a8154c69b5d6ebbf39c37b5bbda5b3f2ae040

    Compilation was done against ROOT v6-24-00.<br>
    The versions for other programs was selected from conan configuration of main programs.<br>

6. **Changes to configuration**

    To have possibility to compile all necessary programs without conan there was a lot of changes to CMake configuration files to add dependences was provided by conan before.<br>
    The most questionable changes was made for Daquiri and EFU:
    - On Ubuntu 20.04 for _EFU_ and _logical-geometry_ the `GTestColor` enum in 'src/test/TestBase.h' file was disabled since it was provided in googletest
    - Also, the _verso_ program on Ubuntu 18.04 crashed on the first use of `TFile`. As a workaround, a redundant bare `TFile` object creation & destruction on start program was added.

7. **Running**

    Before using installed programs user should to run "sw-env.sh" script before working with programs: `source ~/sw/sw-env.sh`.<br>
    It does:
    - Runs _ROOT_ enviroment script (**root/bin/thisroot.sh**)
    - Incudes to **LD_LIBRARY_PATH** (path for library files) and **PATH** (path for executable files) folders for installed programs
    - Set **ESSDAQROOT** variable needed for _ESSDAQ_ program.
    - Adds alias for kafka_run script
    - Adds aliases for essdaq and daquiri scripts for each detector in essdaq/detector directory
    
    So, work procedure looks like:
    ```
    source ~/sw/sw-env.sh
    vmmsc
    kafka_start
    kafka_check
    essdaq_gdgem_start
    daquiri_gdgem_start
    essdaq_gdgem_stop
    kafka_stop
    ```
