#!/usr/bin/env bash

export SWPATH="`realpath ~/sw`"
export SHPATH="`realpath ./install`"

. "$SHPATH"/_preinstall.sh

mkdir -p "$SWPATH"

##################################################
# ROOT
##################################################
bash "$SHPATH"/install_root.sh "$SWPATH" 6-24-00
. "${SWPATH}/root/bin/thisroot.sh"

##################################################
# VMMSC
##################################################
. "$SHPATH"/install_vmmsc.sh

##################################################
# Common packages
##################################################
. "$SHPATH"/install_fmt.sh
. "$SHPATH"/install_nlohmann_json.sh
. "$SHPATH"/install_asio.sh
. "$SHPATH"/install_concurrentqueue2.sh
. "$SHPATH"/install_graylog-logger.sh

##################################################
# VMM-SDAT
##################################################
. "$SHPATH"/install_vmm-sdat.sh

##################################################
# ESSDAQ
##################################################
. "$SHPATH"/install_h5cpp.sh
. "$SHPATH"/install_date.sh
. "$SHPATH"/install_qplot.sh
. "$SHPATH"/install_logical-geometry.sh
. "$SHPATH"/install_spdlog.sh
. "$SHPATH"/install_cli11.sh
. "$SHPATH"/install_trompeloeil.sh
. "$SHPATH"/install_flatbuffers.sh
. "$SHPATH"/install_streaming-data-types1.sh
. "$SHPATH"/install_streaming-data-types2.sh
. "$SHPATH"/install_qt-color-widgets.sh
. "$SHPATH"/install_readerwriterqueue.sh

. "$SHPATH"/install_essdaq.sh

##################################################
# ESSDAQ/Kafka
##################################################
. "$SHPATH"/install_kafka.sh

##################################################
# ESSDAQ/Daquiri
##################################################
. "$SHPATH"/install_daquiri.sh

##################################################
# ESSDAQ/EFU (event-formation-unit)
##################################################
. "$SHPATH"/install_efu.sh

##################################################
# ESSDAQ/Grafana
##################################################
. "$SHPATH"/install_grafana.sh

##################################################
# Scripts with enviroment variables
##################################################
. "$SHPATH"/install_sw-env.sh

